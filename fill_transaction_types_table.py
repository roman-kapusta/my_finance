from app.app import app
from app.database.models import db, TransactionType

types = 'Apartment Car Food Entertainment Internet Clothes Medication Household Goods'

with app.app_context():
	for t in types.split():
		db.session.add(TransactionType(t))

	db.session.commit()
