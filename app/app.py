from flask import Flask, render_template
from flask.ext import restful
from database.models import db
import resources

def create_app():
	app = Flask(__name__)
	# config
	import os
	app.config.from_object(os.environ['APP_SETTINGS'])
	db.init_app(app)
	resources.api.init_app(app)
	return app



def setup_database(app):
	with app.app_context():
		db.create_all()

	
app = create_app()

# setup_database(app)

@app.route('/')
def test():
	return render_template('index.html')
	# return 'Test\n'


