from sys import stdout
import os

class NullOutMixin:
	def print_out_self(self):
		with open(os.devnull, 'w') as f:
			stdout = f
			stdout.write('%r' % self)