from models import Transaction, TransactionType, db
from datetime import datetime, timedelta
import calendar
from sqlalchemy.sql import extract


class BadPeriodError(Exception):
	pass

keys = ['id', 'amount', 'scope', 'created_on', 'description', 'transaction_type']


class QueryModelsHelper:
	def get_transactions_with_type_for_period(self, json):
		if 'year' not in json or 'month' not in json:
			raise BadPeriodError

		year = json['year']
		month = json['month']

		query_result = db.session.query(Transaction.id, 
										Transaction.amount, 
										Transaction.scope, 
										Transaction.created_on,
										Transaction.description,
										TransactionType.transaction_type).\
									join(TransactionType).\
									filter(Transaction.deleted_on == None).\
									filter(extract('year', Transaction.created_on) == year).\
									filter(extract('month', Transaction.created_on) == month).\
									all()

		response = []

		for res in query_result:
			response.append(dict(zip(keys, res)))

		return response


	def get_transactions_with_type_by_period(self, period):
		query_result = None
		response = []

		if period == 0:
			query_result = db.session.query(Transaction.id, 
									Transaction.amount, 
									Transaction.scope, 
									Transaction.created_on,
									Transaction.description,
									TransactionType.transaction_type).\
			join(TransactionType).filter(Transaction.deleted_on == None).all()
		else:

			dates = self._get_min_and_max_dates_by_period(period)

			query_result = db.session.query(Transaction.id, 
				Transaction.amount, 
				Transaction.scope, 
				Transaction.created_on,
				Transaction.description,
				TransactionType.transaction_type).\
			join(TransactionType).\
			filter(Transaction.created_on.between(dates[0], dates[1])).\
			filter(Transaction.deleted_on == None).all()

		for res in query_result:
			response.append(dict(zip(keys, res)))

		return response


	def _get_min_and_max_dates_by_period(self, period):
		min_day = None
		max_day = None
		today = datetime.today()

		if period == 0:
			return Transaction.query.filter_by(deleted_on=None)

		elif period == 1:			
			min_day = datetime(today.year, today.month, today.day)
			max_day = min_day + timedelta(days=1)

		elif period == 7:
			week_days = self._get_current_week_days()
			min_day = week_days[0]
			max_day = week_days[1]

		elif period == 31:
			min_day = datetime(today.year, today.month, 1)
			max_day = datetime(today.year, 
				today.month, 
				calendar.monthrange(today.year, today.month)[1])

		elif period == 365:
			min_day = datetime(today.year, 1, 1)
			max_day = datetime(today.year + 1, 1, 1)
			
		else:
			raise BadPeriodError

		return (min_day, max_day)


	def _get_current_week_days(self):
		today = datetime.today()
		day = datetime(today.year, today.month, today.day)
		day_of_week = day.weekday()

		to_beginning_of_week = timedelta(days=day_of_week)
		beginning_of_week = day - to_beginning_of_week
		to_end_of_week = timedelta(days=7 - day_of_week)
		end_of_week = day + to_end_of_week
		
		return (beginning_of_week, end_of_week)
