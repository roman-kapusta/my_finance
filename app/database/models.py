from datetime import datetime, timedelta
from .null_out_mixin import NullOutMixin
from passlib.apps import custom_app_context as pwd_context
from flask.ext.sqlalchemy import SQLAlchemy

db = SQLAlchemy()



class TransactionType(db.Model, NullOutMixin):

	__tablename__ = 'transaction_types'

	id = db.Column(db.Integer, primary_key=True)
	transaction_type = db.Column(db.String, unique=True, nullable=False)

	def __init__(self, transaction_type):
		self.transaction_type = transaction_type

	# def __repr__(self):
	# 	return 'Type: %s' % self.transaction_type

	def get_dict(self):
		return self.__dict__


class Transaction(db.Model, NullOutMixin):

	__tablename__ = 'transactions'

	id = db.Column(db.Integer, primary_key=True)
	amount = db.Column(db.Integer, nullable=False)
	scope = db.Column(db.Integer, nullable=False)
	description = db.Column(db.String, nullable=True)
	created_on = db.Column(db.DateTime, nullable=False, index=True, default=datetime.utcnow)
	deleted_on = db.Column(db.DateTime, nullable=True)
	transaction_type_id = db.Column(db.Integer, db.ForeignKey('transaction_types.id'), nullable=False)
	transaction_type = db.relationship('TransactionType', 
		backref=db.backref('transactions', lazy='dynamic'))

	# def __repr__(self):
	# 	return """Transaction amount: {}, 
	# 				scope: {},  
	# 				transaction_type: {}, 
	# 				description: {}, 
	# 				created_on: {},
	# 				deleted_on: {}""".format(
	# 					str(self.amount),
	# 					str(self.scope),
	# 					str(self.transaction_type),
	# 					self.description,
	# 					str(self.created_on),
	# 					str(self.deleted_on))

	def get_dict(self):
		response = {}

		for key, value in self.__dict__.items():
			if key == 'transaction_type':
				response[key] = value.get_dict()
			else:
				response[key] = value

		return response



class UserExistError(Exception):
	pass

class User(db.Model):

	__tablename__ = 'users'

	id = db.Column(db.Integer, primary_key=True)
	user_name = db.Column(db.String, index=True, unique=True, nullable=False)
	password_hash = db.Column(db.String, nullable=False)

	def __init__(self, user_name, password):
		self.user_name = user_name
		self._hash_password(password)

	def _hash_password(self, password):
		self.password_hash = pwd_context.encrypt(password)

	def verify_password(self, password):
		return pwd_context.verify(password, self.password_hash)
































			
