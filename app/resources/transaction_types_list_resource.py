from flask.ext.restful import Resource, marshal
from flask import jsonify
from common import auth, tt_fields
from app.database.models import TransactionType

class TransactionTypesListResource(Resource):

	decorators = [auth.login_required]

	def get(self):		
		types = TransactionType.query.all()

		trTypesDictList = []

		for tt in types:
			trTypesDictList.append(tt.get_dict())

		return {'transaction_types': map(lambda tt: marshal(tt, tt_fields), trTypesDictList)} 