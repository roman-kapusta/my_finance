from flask.ext.restful import Resource, marshal_with, marshal
from app.database.models import Transaction, TransactionType, db 
from flask import request, abort, jsonify
from datetime import datetime
from common import auth, transaction_fields
import aniso8601 as DateParser


class TransactionResource(Resource):
	decorators = [auth.login_required]

	@marshal_with(transaction_fields, envelope='transaction')
	def get(self, transaction_id):
		t = Transaction.query.get(transaction_id)

		if not t:
			abort(404)

		return t.get_dict()

	def post(self):
		if not request.json:
			abort(400)

		tt = TransactionType.query.get(request.json['transaction_type'])

		print 'Transaction type {}'.format(tt)

		if not tt:
			return {'message': 'Transaction type not found'}, 400


		new_transaction = Transaction(amount=request.json['amount'],
			scope=request.json['scope'],
			description=request.json.get('description', None),
			created_on=self._get_created_on_date(request),
			transaction_type=tt)

		db.session.add(new_transaction)
		db.session.commit()

		new_transaction.print_out_self()

		return {'transaction': marshal(new_transaction.get_dict(), transaction_fields)} 


	def _get_created_on_date(self, request):
		import aniso8601

		date = request.json.get('created_on', datetime.utcnow())
		return date if isinstance(date, datetime) else aniso8601.parse_datetime(date)







		