from flask.ext.httpauth import HTTPBasicAuth
from app.database.models import User
from flask import make_response, jsonify
from flask.ext.restful import fields

auth = HTTPBasicAuth()

@auth.verify_password
def verify_password(username, password):	
	user = User.query.filter_by(user_name=username).first()

	if not user or not user.verify_password(password):
		return False

	return True

@auth.error_handler
def unauthorized():
    return make_response(jsonify( { 'message': 'Unauthorized access' } ), 403)


tt_fields = {
	'id': fields.Integer,
	'transaction_type': fields.String
}


transaction_fields = {
	'id': fields.Integer,
	'scope': fields.Integer,
	'amount': fields.Integer,
	'created_on': fields.DateTime(dt_format='iso8601'),
	'description': fields.String,
	'transaction_type': fields.String
}