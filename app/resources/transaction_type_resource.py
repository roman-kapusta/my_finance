from flask.ext.restful import Resource, marshal_with
from flask import request, abort
from app.database.models import TransactionType, db
from sqlalchemy.exc import IntegrityError
from common import auth, tt_fields

class TransactionTypeResource(Resource):
	
	decorators = [auth.login_required]

	@marshal_with(tt_fields, envelope='transaction_type')
	def get(self, type_id):		
		tt = TransactionType.query.get(type_id)

		if not tt:
			abort(404)

		return tt.get_dict()

	@marshal_with(tt_fields, envelope='transaction_type')
	def post(self):
		if not request.json or 'type' not in request.json:
			abort(400)

		new_type = TransactionType(request.json['type'])
				
		db.session.add(new_type)

		response = None
				
		try:
			db.session.commit()
		except IntegrityError, e:
			db.session.rollback()
			message = e.message.split()[1:]
			response = {'message': ' '.join(message)}
			response.status_code = 403
		else:
			new_type.print_out_self()

			response = new_type.get_dict()
		
		return response
		