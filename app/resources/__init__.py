from flask.ext.restful import Api

api = Api()

from transaction_types_list_resource import TransactionTypesListResource
from transaction_resource import TransactionResource
from transactions_list_resource import TransactionsListResource
from transaction_type_resource import TransactionTypeResource


api.add_resource(
		TransactionTypeResource, 
		'/transaction-type', 
		'/transaction-type/<int:type_id>')

api.add_resource(
	TransactionTypesListResource, 
	'/transaction-types/')

api.add_resource(
	TransactionResource, 
	'/transaction', 
	'/transaction/<int:transaction_id>')

api.add_resource(
	TransactionsListResource,
	'/transactions',
	'/transactions/<int:period>')















