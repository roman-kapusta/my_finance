from flask.ext.restful import Resource, marshal
from flask import abort, request
from common import auth, transaction_fields
from app.database.query_models_helper import QueryModelsHelper, BadPeriodError


class TransactionsListResource(Resource):

	decorators = [auth.login_required]

	def get(self, period):		
		helper = QueryModelsHelper()

		transactions = None
		try:
			transactions = helper.get_transactions_with_type_by_period(period)
		except BadPeriodError:
			abort(404)

		return {'transactions': map(lambda t: marshal(t, transaction_fields), transactions)}

	def get(self):
		if not request.json:
			abort(404)

		helper = QueryModelsHelper()

		transactions = None

		try:
			transactions = helper.get_transactions_with_type_for_period(request.json)
		except BadPeriodError:
			abort(404)

		return {'transactions': map(lambda t: marshal(t, transaction_fields), transactions)}





		
