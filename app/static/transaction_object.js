// Describes Transaction object
function TransactionObject(type, scope, amount, date, description) {
    var self = this;

    self.transaction_type = type;
    self.scope = scope;
    self.amount = amount;
    self.created_on = date;
    self.description = description;
}