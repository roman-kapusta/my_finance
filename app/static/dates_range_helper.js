function DateObj(year, month){
	var self = this;

	self.year = parseInt(year);
	self.month = month + 1;
}

var monthDisplayNameMap = {
	0: 'January',
	1: 'February',
	2: 'March',
	3: 'April',
	4: 'May',
	5: 'June',
	6: 'July',
	7: 'August',
	8: 'September',
	9: 'October',
	10: 'November',
	11: 'December'
}

function DateRange() {
	var self = this;

	self.getDatesRange = function(){
		var startDate = new Date(2014, 11, 01);
		var endDate = new Date();
		var range = [];
		var years = [];
		var yearMonths = {};
		var currentDate = startDate;	

		while(currentDate <= endDate){
			range.push(new Date(currentDate));
			currentDate.addMonth();
		}

		range.forEach(function(e){
			var year = e.getFullYear();
			if (!years.contains(year)) {
				years.push(year);
			}
		});		

		years.forEach(function(year){
			yearMonths[year.toString()] = [];
		});

		for(var year in yearMonths){
			range.forEach(function(date){
				if (date.getFullYear().toString() == year) {
					yearMonths[year].push(date.getMonth());
				};
			});
		}

		return yearMonths;
	}
}