Date.prototype.addMonth = function() {
		var self = this;
		self.setMonth(this.getMonth() + 1);
}

Array.prototype.contains = function (v) {
		var self = this;

    	return self.indexOf(v) > -1;
}

Date.prototype.today = function() {
	var self = this;

	var dd = this.getDate();
	var mm = this.getMonth() + 1;
	var yy = this.getFullYear();

	if (dd < 10) {
		dd = "0" + dd;
	}

	if (mm < 10) {
		mm = "0" + mm;
	}

	return dd + "/" + mm + "/" + yy;
}