var datePattern = /(\d{2})\/(\d{2})\/(\d{4})/;

// Returns new date string in "yyyy-mm-ddThh:mm:ss" format from string format "dd/mm/yyyy"
function DateFromString(string) {
	var date = string.replace(datePattern, "$3-$2-$1");

	return new Date(date);
}