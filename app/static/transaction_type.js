// Describes TransactionType object
function TransactionType(transactionType) {
    var self = this;

    self.transactionId = transactionType.id;
    self.transactionType = transactionType.transaction_type;
}